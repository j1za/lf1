<!DOCTYPE html>
<html <?php language_attributes(); ?>>

<head>
    <?php get_template_part( 'template-parts/head' ); ?>
</head>

<style>
body.page,
body.home {
    background: center/cover no-repeat url('<?php the_field('imagem'); ?>');
}

body.blog {
    background: center/cover no-repeat url('<?php the_field('imagem', 55); ?>');
}

body.page-template-template-about {
    background: center/cover no-repeat url('<?php the_field('imagem'); ?>');
}

body.page-template-template-join {
    background: center/cover no-repeat url('<?php the_field('imagem'); ?>');
}

body.privacy-policy {
    background: center/cover no-repeat url('<?php the_field('imagem'); ?>');
}

body.page-id-137 {
    background: center/cover no-repeat url('<?php the_field('imagem'); ?>');
}
</style>

<body <?php body_class(); ?>>
    <div class="wrapper" id="content">
        <div id="canvas"></div>
        <div id="water-ripples">
            <?php if(is_page()): ?>
            <img crossorigin data-sampler="planeTexture" src="<?php the_field('imagem'); ?>" />
            <?php elseif(!is_singular() || !is_page() || !is_archive() || !is_singular()): ?>
            <img crossorigin data-sampler="planeTexture" src="<?php the_field('imagem', 55); ?>" />
            <?php elseif(!is_page() && !is_archive() && !is_single() || is_archive()): ?>
            <picture>
                <source crossorigin data-sampler="planeTexture"
                    srcset="<?php echo get_theme_file_uri( 'dist/images/LF1 Gradient BG NEW 5.webp'); ?>">
                <img crossorigin data-sampler="planeTexture"
                    src="<?php echo get_theme_file_uri( 'dist/images/LF1 Gradient BG NEW 5.jpg'); ?>" />
            </picture>
            <?php elseif(is_single() && !is_singular('openings')): ?>
            <picture>
                <source crossorigin data-sampler="planeTexture"
                    srcset="<?php echo get_theme_file_uri( 'dist/images/LF1 Gradient BG NEW 4.webp' ); ?>">
                <img crossorigin data-sampler="planeTexture"
                    src="<?php echo get_theme_file_uri( 'dist/images/LF1 Gradient BG NEW 4.jpg' ); ?>" />
            </picture>
            <?php elseif(is_singular('openings')): ?>
            <picture>
                <source crossorigin data-sampler="planeTexture"
                    srcset="<?php echo get_theme_file_uri( 'dist/images/LF1 Gradient BG NEW 6.webp' ); ?>">
                <img crossorigin data-sampler="planeTexture"
                    src="<?php echo get_theme_file_uri( 'dist/images/LF1 Gradient BG NEW 6.jpg' ); ?>" />
            </picture>

            <?php endif; ?>
        </div>
        <div class="content">
            <header class="header">
                <div class="header__inner">
                    <div class="logo">
                        <a class="logo__pc" href="<?php echo esc_url( home_url( '' ) ); ?>" rel="home">
                            <img src="<?php the_field('logo', 'option') ?>" alt="">
                        </a>
                        <a class="logo__phone" href="<?php echo esc_url( home_url( '' ) ); ?>" rel="home">
                            <img src="<?php the_field('logo_mobile', 'option') ?>" alt="">
                        </a>
                    </div>
                    <div class="header__menu">
                        <?php wp_nav_menu( [
                                'theme_location'=>  'headerMenu',
                                'menu'            => 'header_menu',
                                'container'       => 'nav',
                                'container_class' => 'menu',
                                'items_wrap'        => '<ul>%3$s</ul>',
                                'fallback_cb'     => 'wp_page_menu',
                        ] ); ?>
                        <div class="footer__newsletter">
                            <div class="footer__newsletter-inner">
                                <?php echo do_shortcode('[contact-form-7 id="19" title="Newsletter in footer"]') ?>
                            </div>
                        </div>
                        <div class="footer__navigations">

                            <?php if(get_field('add_menu_block_phone', 'option')): ?>
                            <?php while(has_sub_field('add_menu_block_phone', 'option')): ?>

                            <div class="box">
                                <div class="box__inner">
                                    <div class="box__title">
                                        <?php the_sub_field('title'); ?>
                                    </div>
                                    <div class="box__info">
                                        <?php the_sub_field('short_information'); ?>
                                    </div>
                                </div>
                            </div>
                            <?php endwhile; ?>
                            <?php endif; ?>
                        </div>
                    </div>


                    <div class="menu-toggle">
                        <div class="hamburger">
                            <span></span>
                            <span></span>
                            <span></span>
                        </div>
                        <div class="cross">
                            <span></span>
                            <span></span>
                        </div>
                    </div>
                </div>
            </header>