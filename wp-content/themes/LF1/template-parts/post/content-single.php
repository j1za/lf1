<?php get_template_part( 'template-parts/content', 'information'); ?>

<section class="post-content">
    <div class="container">
        <div class="post-content__inner <?php if(is_page(137)): ?>impri <?php elseif(is_page(119)): ?>p-p<?php endif; ?>">
            <?php the_content(); ?>
        </div>
    </div>
</section>