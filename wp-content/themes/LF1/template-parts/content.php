<div class="posts__box">
    <a href="<?php echo get_permalink() ?>" class="posts__box-img">
    <?php the_post_thumbnail() ?>
    </a>
    <div class="posts__box-inner">
        <a href="<?php echo get_permalink() ?>">
            <h3 class="posts__box-title">
                <?php the_title(); ?>
            </h3>
        </a>
        <h4 class="posts__box-date">
            <?php the_author_posts_link() ?>, <?php the_date('Y-m-d'); ?>
        </h4>
        <a href="<?php echo get_permalink() ?>">
            <div class="posts__box-text">
                <?php the_excerpt(); ?>
            </div>
        </a>
    </div>
</div>