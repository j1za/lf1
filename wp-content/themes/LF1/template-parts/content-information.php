<section class="hinformation">
    <div class="container">
        <div class="hinformation__inner">
            <?php if(is_single()): ?>
                <h4 class="hinformation__date">
                    <?php the_author_posts_link() ?>, <?php the_date('Y-m-d'); ?>
                </h4>
                <?php the_title( '<h1 class="hinformation__title">', '</h1>' ); ?>
                <?php else : ?>
                    <?php if(is_archive()): ?>
                    <h2 class="hinformation__title"><?php wp_title('')?></h2>
                    
                    <?php elseif(is_page(78)) : ?>
                    <h4 class="hinformation__title--about"><?php wp_title('')?></h4>

                    <?php elseif(is_page(95)) : ?>
                    <h4 class="hinformation__title--join"><?php wp_title('')?></h4>

                    <?php elseif(!is_page() && !is_archive()) : ?>
                    <h4 class="hinformation__title--blog"><?php wp_title('')?></h4>

                    <?php elseif(is_page(119)): ?>
                        <h4 class="hinformation__title--pp"><?php wp_title('')?></h4>

                    <?php elseif(is_page(137)): ?>
                        <h2 class="hinformation__title--imp"><?php wp_title('')?></h2>

                    <?php else : ?>
                    <h4 class="hinformation__title"><?php wp_title('')?></h4>
                    <?php endif; ?>

                    <?php if(!is_page() && !is_archive()): ?>
                    <div class="hinformation__text">
                     <?php the_field('add_text', 55) ?>
                    </div>
                    <?php endif; ?>
            <?php endif; ?>

            <?php if(is_page() && !is_page(119) && !is_page(78)): ?>
                <div class="hinformation__text">
                    <?php the_field('add_text') ?>
                </div>
            <?php elseif(is_page(78)): ?>
            <div class="hinformation__text--about">
                <?php the_field('add_text') ?>
            </div>
            <?php elseif(is_page(119)): ?>
                <div class="hinformation__text--pp">
                    <?php the_field('add_text') ?>
                </div>
            
            <?php endif; ?>

        </div>
    </div>
</section>