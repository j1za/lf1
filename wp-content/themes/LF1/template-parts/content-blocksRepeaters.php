<section class="blocks-repeaters <?php if(is_page(95)) : ?> blocks-repeaters--join<?php endif; ?>">
    <div class="container">
        <div class="blocks-repeaters__inner">
            <?php if(is_page(78)) : ?>

            <div class="blocks-repeaters__title">
                <?php the_field('title1'); ?>
            </div>
            <div class="blocks-repeaters__items">

                <?php if(get_field('add_block_with_info')): ?>
                <?php while(has_sub_field('add_block_with_info')): ?>

                <div class="blocks-repeaters__item">
                    <?php the_sub_field('text'); ?>
                </div>

                <?php endwhile; ?>
                <?php endif; ?>
            </div>
            <?php elseif(95) : ?>

            <div class="blocks-repeaters__title blocks-repeaters__title--join">
                <?php the_field('add_title'); ?>
            </div>

            <div class="blocks-repeaters__items">
                <?php
												$loop = new WP_Query( array(
												  'posts_per_page' => -1,
												  'post_type' => 'openings',
												  'orderby'  => 'date',
												  'order'    => 'ASC',
												) );
												?>
                <?php while ( $loop->have_posts() ) : $loop->the_post();?>


                <div class="blocks-repeaters__item blocks-repeaters__item--join">
                    <a class="blocks-repeaters__link" href="<?php echo get_permalink(); ?>"></a>
                    <p><?php the_title(); ?></p>
                </div>


                <?php endwhile; ?>


            </div>
            <?php endif; ?>
        </div>
    </div>
</section>