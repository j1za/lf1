// import ScrollReveal from 'scrollreveal';
const burgerMenu = document.querySelector('.menu-toggle'),
    HeaderMenu = document.querySelector('.header');
burgerMenu.addEventListener('click', () => {
    burgerMenu.classList.toggle('open');
    document.body.classList.toggle('body--hidden');
    HeaderMenu.classList.toggle('header--open');
});

// let textWrapper = document.querySelectorAll('em');
// for(let i = 0; i < textWrapper.length; i++) {
//     textWrapper[i].innerHTML = `<strong>${textWrapper[i].textContent.replace(/\S/g, "<span class='letter'>$&</span>")}</strong>`;
// }
// const runAnimtion = {
//     footerBoxTitle: () => {
//         anime({
//             targets: '.footer__navigations .box__title em .letter',
//             scale: [ 2, 1 ],
//             opacity: [ 0, 1 ],
//             translateZ: 0,
//             easing: "easeOutExpo",
//             duration: 1000,
//             delay: (el, i) => 100 * i
//         });
//     },
//     blockMore: () => {
//         anime({
//             targets: '.bmore__inner .box__title em .letter',
//             scale: [ 2, 1 ],
//             opacity: [ 0, 1 ],
//             translateZ: 0,
//             easing: "easeOutExpo",
//             duration: 1500,
//             delay: (el, i) => 100 * i
//         });
//     },
//     mainText: () => {
//         anime({
//             targets: '.main__text em .letter',
//             opacity: [ 0, 1 ],
//             rotateY: [ -90, 0 ],
//             easing: "easeOutExpo",
//             duration: 1500,
//             delay: (el, i) => 90 * i
//         });
//     },
//     mainInformation: () => {
//         anime({
//             targets: '.hinformation__inner em .letter',
//             scale: [ 2, 1 ],
//             opacity: [ 0, 1 ],
//             translateZ: 0,
//             easing: "easeOutExpo",
//             duration: 1500,
//             delay: (el, i) => 100 * i
//         });
//     },
//     blocksRepeaters: () => {
//         anime({
//             targets: '.blocks-repeaters em .letter',
//             scale: [ 2, 1 ],
//             opacity: [ 0, 1 ],
//             translateZ: 0,
//             easing: "easeOutExpo",
//             duration: 1500,
//             delay: (el, i) => 100 * i
//         });
//     }
// };
// let widnowWodth = window.matchMedia('(max-width: 767px)');
// let footerLetters = document.querySelector('.footer__inner'),
//     bmoreLetters = document.querySelector('.bmore__inner'),
//     mainLetters = document.querySelector('.main__text'),
//     informationLetters = document.querySelector('.hinformation__text'),
//     repeatersLetters = document.querySelector('.blocks-repeaters__inner');
// let footerLettersCount = 0,
//     bmoreLettersCount = 0,
//     mainLettersCount = 0,
//     informationLettersCount = 0,
//     repeatersLettersCount = 0;
// window.addEventListener('load', () => {
//     if(widnowWodth.matches === false) {
//         const observer = new IntersectionObserver(
//             entries => {
//                 entries.forEach(entry => {
//                     if(entry.intersectionRatio >= 0.2) {
//                         if(entry.target.className === footerLetters.className && footerLettersCount === 0) {
//                             runAnimtion.footerBoxTitle();
//                             footerLettersCount += 1;
//                         }
//                         if(bmoreLetters !== null && entry.target.className === bmoreLetters.className && bmoreLettersCount === 0) {
//                             runAnimtion.blockMore();
//                             bmoreLettersCount += 1;
//                         }
//                         if(mainLetters !== null && entry.target.className === mainLetters.className && mainLettersCount === 0) {
//                             runAnimtion.mainText();
//                             mainLettersCount += 1;
//                         }
//                         if(informationLetters !== null && entry.target.className === informationLetters.className && informationLettersCount === 0) {
//                             runAnimtion.mainInformation();
//                             informationLettersCount += 1;
//                         }
//                         if(repeatersLetters !== null && entry.target.className === repeatersLetters.className && repeatersLettersCount === 0) {
//                             runAnimtion.blocksRepeaters();
//                             repeatersLettersCount += 1;
//                         }
//                     }
//                 });
//             }, { threshold: 0.2 }
//         );
//         observer.observe(footerLetters);
//         if(bmoreLetters) {
//             observer.observe(bmoreLetters);
//         }
//         if(mainLetters) {
//             observer.observe(mainLetters);
//         }
//         if(informationLetters) {
//             observer.observe(informationLetters);
//         }
//         if(repeatersLetters) {
//             observer.observe(repeatersLetters);
//         }
//     }
// });
// window.addEventListener('resize', () => {
//     if(widnowWodth.matches === true) {
//         for(let i = 0; i < textWrapper.length; i++) {
//             textWrapper[i].innerHTML = textWrapper[i].textContent.concat();
//         }
//     }
//     if(widnowWodth.matches === false) {
//         for(let i = 0; i < textWrapper.length; i++) {
//             textWrapper[i].innerHTML = `<strong>${textWrapper[i].textContent.replace(/\S/g, "<span class='letter'>$&</span>")}</strong>`;
//         }
//         const observer = new IntersectionObserver(
//             entries => {
//                 entries.forEach(entry => {
//                     if(entry.intersectionRatio >= 0.2) {
//                         if(entry.target.className === footerLetters.className && footerLettersCount === 0) {
//                             runAnimtion.footerBoxTitle();
//                             footerLettersCount += 1;
//                         }
//                         if(bmoreLetters !== null && entry.target.className === bmoreLetters.className && bmoreLettersCount === 0) {
//                             runAnimtion.blockMore();
//                             bmoreLettersCount += 1;
//                         }
//                         if(mainLetters !== null && entry.target.className === mainLetters.className && mainLettersCount === 0) {
//                             runAnimtion.mainText();
//                             mainLettersCount += 1;
//                         }
//                         if(informationLetters !== null && entry.target.className === informationLetters.className && informationLettersCount === 0) {
//                             runAnimtion.mainInformation();
//                             informationLettersCount += 1;
//                         }
//                         if(repeatersLetters !== null && entry.target.className === repeatersLetters.className && repeatersLettersCount === 0) {
//                             runAnimtion.blocksRepeaters();
//                             repeatersLettersCount += 1;
//                         }
//                     }
//                 });
//             }, { threshold: 0.2 }
//         );
//         observer.observe(footerLetters);
//         if(bmoreLetters) {
//             observer.observe(bmoreLetters);
//         }
//         if(mainLetters) {
//             observer.observe(mainLetters);
//         }
//         if(informationLetters) {
//             observer.observe(informationLetters);
//         }
//         if(repeatersLetters) {
//             observer.observe(repeatersLetters);
//         }
//     }
// });

// window.sr = new ScrollReveal({ reset: false }).reveal('.hinformation__inner p,.hinformation__inner h1,.hinformation__inner h2,.hinformation__inner h4,.blocks-repeaters__title,.blocks-repeaters__item,.nav-links, .footer__inner,.post-content__inner p,.post-content__inner li,.post-content__inner pre,.post-content__inner h1,.post-content__inner h2,.post-content__inner h3,.post-content__inner h4', {
//     origin: 'bottom',
//     opacity: 0,
//     easing: 'cubic-bezier(.42,0,1,1)',
//     delay: 150,
//     duration: 600,
//     mobile: false,
//     viewOffset: { top: 100 }
// });
// window.sr = new ScrollReveal({ reset: false }).reveal('.bmore__inner .box,.bmore__inner hr,.posts__box,body.home .header,.main__inner,.main__text,hr', {
//     opacity: 0,
//     easing: 'cubic-bezier(.42,0,1,1)',
//     delay: 150,
//     interval: 300,
//     duration: 600,
//     mobile: false,
//     viewOffset: { top: 100 }
// });
// window.sr = new ScrollReveal({ reset: false }).reveal('.main__text,hr,body.home .header', {
//     opacity: 0,
//     easing: 'cubic-bezier(.42,0,1,1)',
//     delay: 200,
//     interval: 450,
//     duration: 1000,
//     mobile: false,
//     viewOffset: { top: 100 }
// });