        </div>
        <!--close content class tag-->
        <footer class="footer">
            <div class="container">
                <div class="footer__inner">
                    <div class="footer__newsletter">
                        <div class="footer__newsletter-inner">
                            <?php echo do_shortcode('[contact-form-7 id="19" title="Newsletter in footer"]') ?>
                        </div>

                        <div class="footer__sub">
                            <?php the_field('signature', 'option'); ?>
                        </div>
                    </div>
                    <div class="footer__navigations">

                        <?php if(get_field('add_menu_block', 'option')): ?>
                        <?php while(has_sub_field('add_menu_block', 'option')): ?>
                        
                        <div class="box">
                            <div class="box__inner">
                                <div class="box__title">
                                   <em><?php the_sub_field('title'); ?></em> 
                                </div>
                                <div class="box__info">
                                    <?php the_sub_field('short_information'); ?>
                                </div>
                            </div>
                            <?php if(get_row_index() == 3){ ?>
                            <div class="logo">
                                <a href="<?php echo esc_url( home_url( '' ) ); ?>" rel="home">
                                    <img src="<?php the_field('logo', 'option') ?>" alt=""></a>
                            </div>
                            <?php
                            } ?>

                        </div>
                        <?php endwhile; ?>
                        <?php endif; ?>
                    </div>
                </div>
                <div class="footer__sub">
                    <?php the_field('signature', 'option'); ?>
                </div>
            </div>

        </footer>
        </div>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/dat-gui/0.7.7/dat.gui.min.js"></script>
        <!--close wrapper class tag-->
        <?php wp_footer(); ?>
        </body>

        </html>