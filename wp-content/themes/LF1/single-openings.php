<?php get_header(); ?>

<main>
    <?php while ( have_posts() ) : the_post(); ?>

    <section class="hinformation">
        <div class="container">
            <div class="hinformation__inner">
                <h4 class="hinformation__date">
                    JOB DETAILS
                </h4>
                <?php the_title( '<h1 class="hinformation__title--opennings">', '</h1>' ); ?>

            </div>
        </div>
    </section>

    <section class="post-content post-content--opennings">
        <div class="container">
            <div class="post-content__inner">
                <?php the_content(); ?>
            </div>
        </div>
    </section>

    <?php endwhile; ?>
</main>

<?php get_footer(); ?>