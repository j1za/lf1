<?php get_header(); ?>

        
            <?php if ( have_posts() ) : ?>
                <?php get_template_part( 'template-parts/content', 'information'); ?>
                <section class="posts">
                    <div class="container">
                <?php while ( have_posts() ) : the_post(); ?>
                   
                            <?php get_template_part( 'template-parts/content', get_post_format() ); ?>
                        
                <?php endwhile; ?>
                    </div>
                </section>
                <section class="pagination">
                    <div class="container">
                        <?php the_posts_pagination( array(
	                        'mid_size' => 1,
	                        'prev_text' => __( '<', 'textdomain' ),
	                        'next_text' => __( '>', 'textdomain' ),
                        ) ); ?>
                    </div>
                </section>
                
            <?php else : ?>

                <?php get_template_part( 'template-parts/content', 'none' ); ?>

        <?php endif; ?>
       

<?php get_footer(); ?>
