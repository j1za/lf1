<?php get_header(); ?>
<section class="main">
    <div class="container">
        <div class="main__inner">
            <div class="main__text">
                <?php the_field('text'); ?>
            </div>
            <hr>
        </div>
    </div>
</section>

<section class="bmore">
    <div class="container">
        <div class="bmore__inner" data-scroll-container>
            <?php if( have_rows('add_block') ): ?>
            <?php while( have_rows('add_block') ): the_row(); ?>

            <div class="box">
                <div class="box__inner">
                    <div class="box__title">
                        <?php the_sub_field('title'); ?>
                    </div>
                    <div class="box__text">
                        <?php the_sub_field('text'); ?>
                    </div>
                </div>
                <div class="box__button">
                    <span class="mas"><?php the_sub_field('name_button'); ?></span>
                    <a href="<?php the_sub_field('url_for_button'); ?>" name="Hover"
                        class="btn"><?php the_sub_field('name_button'); ?></a>
                </div>
            </div>

            <?php endwhile; ?>
            <?php endif; ?>
            <hr>
        </div>
    </div>
</section>

<?php get_footer(); ?>