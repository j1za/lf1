<?php
/**
* Create custom post types.
 * https://wp-kama.ru/function/register_post_type
 * https://wp-kama.ru/function/register_taxonomy
 *
*/


add_action('init', 'my_custom_init');
function my_custom_init(){
	register_post_type('openings', array(
		'labels'             => array(
			'name'               => 'All openings', // Основное название типа записи
			'parent_item_colon'  => '',
			'menu_name'          => 'Current openings'

		  ),

		'public'             => true,
		'publicly_queryable' => true,
		'show_ui'            => true,
		'show_in_menu'       => true,
		'query_var'          => true,
		'rewrite'            => true,
		'capability_type'    => 'post',
        'menu_icon'          => 'dashicons-groups',
		'has_archive'        => true,
		'hierarchical'       => false,
		'menu_position'      => null,
		'supports'           => array('title','editor','thumbnail')
	) );
}